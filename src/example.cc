/*
 ============================================================================
 Name        : exampleProgram.c
 Author      : buttonfly
 Version     :
 Copyright   : Your copyright notice
 Description : Uses shared library to print greeting
               To run the resulting executable the LD_LIBRARY_PATH must be
               set to ${project_loc}/libsimplenet/.libs
               Alternatively, libtool creates a wrapper shell script in the
               build directory of this program which can be used to run it.
               Here the script will be called exampleProgram.
 ============================================================================
 */

#include <memory>
#include <unistd.h>
#include <stdio.h>
#include <simplethread/simple_thread.h>

int main(int argc, char *argv[]) {
  std::unique_ptr<base::SimpleThread> thr{new base::SimpleThread};

  thr->Init();
  thr->Start();

  getchar();

  thr->PostTask([&]() {
    fprintf(stderr, "Hello\n");
    sleep(1);
  });

  thr->PostTask([&]() {
    int a = 1;
    int b = 2;
    fprintf(stderr, "1 + 2 = %d\n", a + b);
    sleep(1);
  });

  thr->PostTask([&]() {
    int a = 2;
    int b = 3;
    fprintf(stderr, "2 + 3 = %d\n", a + b);
    sleep(1);
  });

  thr->PostTask([&]() {
    int a = 2;
    int b = 3;
    fprintf(stderr, "bye\n");
    sleep(1);
  });


  sleep(10);

  thr->Stop();

  thr.reset(nullptr);
  return 0;
}
