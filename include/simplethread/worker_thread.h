/*
 * simple_thread.h
 *
 *  Created on: Sep 30, 2017
 *      Author: buttonfly
 */

#ifndef GURUM_BASE_WORKER_THREAD_H_
#define GURUM_BASE_WORKER_THREAD_H_

#include <thread>
#include <mutex>
#include <condition_variable>
#include <functional>
#include <list>

namespace base {

class WorkerThread {

using Task=std::function<void()>;

public:
WorkerThread();
  virtual ~WorkerThread();

  int Init(int numof_thread=1);
  int Start();
  void Stop();

  int PostTask(Task task);

private:
  void Run();
  Task GetTask();

private:
  std::list<Task> task_queue_;

  std::list<std::thread> threads_;
  std::mutex lck_;
  std::condition_variable cond_;

  bool started_ = false;
  bool stopped_ = false;

  int numof_thread_=1;
};

} /* namespace base */

#endif /* GURUM_BASE_WORKER_THREAD_H_ */
